﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace sumOfDigits
{
    class Program
    {
        static void Main(string[] args)
        {

           //calculates the sum of number input by the user
            double num;
            double result = 0;
          
            //Prompts user for input
            Console.WriteLine("Enter desired number:");
            num = Convert.ToInt64(Console.ReadLine());
            double number = num;
            //while user input is greater than 0
            while (num > 0)
            {
                result = result + (num % 10);
                num = Math.Floor(num / 10);
            }
            Console.WriteLine("The sum of "+number+ " is: "+result);

            Console.ReadKey();
        }
    }
}
